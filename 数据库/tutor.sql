/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50626
Source Host           : localhost:3306
Source Database       : tutor

Target Server Type    : MYSQL
Target Server Version : 50626
File Encoding         : 65001

Date: 2017-03-19 11:43:59
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for city
-- ----------------------------
DROP TABLE IF EXISTS `city`;
CREATE TABLE `city` (
  `C-id` int(11) NOT NULL,
  `C-name` varchar(20) DEFAULT NULL,
  `C-ProvinceId` int(11) DEFAULT NULL,
  PRIMARY KEY (`C-id`),
  KEY `C-ProinceId` (`C-ProvinceId`),
  CONSTRAINT `C-ProinceId` FOREIGN KEY (`C-ProvinceId`) REFERENCES `province` (`P-id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of city
-- ----------------------------
INSERT INTO `city` VALUES ('1', '开封', '1');
INSERT INTO `city` VALUES ('2', '郑州', '1');

-- ----------------------------
-- Table structure for course
-- ----------------------------
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course` (
  `Course-id` int(11) NOT NULL,
  `Course-name` varchar(255) NOT NULL,
  PRIMARY KEY (`Course-id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of course
-- ----------------------------
INSERT INTO `course` VALUES ('1', '数学');
INSERT INTO `course` VALUES ('2', '英语');
INSERT INTO `course` VALUES ('3', '语文');
INSERT INTO `course` VALUES ('4', '物理');

-- ----------------------------
-- Table structure for friend
-- ----------------------------
DROP TABLE IF EXISTS `friend`;
CREATE TABLE `friend` (
  `F-id` int(11) NOT NULL AUTO_INCREMENT,
  `F-User1Id` int(11) NOT NULL,
  `F-User2Id` int(11) NOT NULL,
  PRIMARY KEY (`F-id`),
  KEY `F-User1Id` (`F-User1Id`),
  KEY `F-User2Id` (`F-User2Id`),
  CONSTRAINT `F-User1Id` FOREIGN KEY (`F-User1Id`) REFERENCES `user` (`U-id`),
  CONSTRAINT `F-User2Id` FOREIGN KEY (`F-User2Id`) REFERENCES `user` (`U-id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of friend
-- ----------------------------
INSERT INTO `friend` VALUES ('1', '1', '2');
INSERT INTO `friend` VALUES ('2', '2', '1');

-- ----------------------------
-- Table structure for message
-- ----------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE `message` (
  `M-id` int(11) NOT NULL AUTO_INCREMENT,
  `M-Message` text NOT NULL,
  `M-Time` datetime NOT NULL,
  `M-FromUserId` int(11) NOT NULL,
  `M-ToUserId` int(11) NOT NULL,
  PRIMARY KEY (`M-id`),
  KEY `M-FromUserId` (`M-FromUserId`),
  KEY `M-ToUserId` (`M-ToUserId`),
  CONSTRAINT `M-FromUserId` FOREIGN KEY (`M-FromUserId`) REFERENCES `user` (`U-id`),
  CONSTRAINT `M-ToUserId` FOREIGN KEY (`M-ToUserId`) REFERENCES `user` (`U-id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of message
-- ----------------------------
INSERT INTO `message` VALUES ('1', '作业写好了吗？', '2017-03-23 15:03:44', '1', '2');
INSERT INTO `message` VALUES ('2', '还没有呢', '2017-03-23 15:04:10', '2', '1');
INSERT INTO `message` VALUES ('3', '加油，认真点', '2017-03-11 15:09:31', '1', '2');

-- ----------------------------
-- Table structure for parttime
-- ----------------------------
DROP TABLE IF EXISTS `parttime`;
CREATE TABLE `parttime` (
  `T-id` int(11) NOT NULL AUTO_INCREMENT,
  `T-content` text,
  `T-UserId` int(11) NOT NULL,
  `T-time` datetime NOT NULL,
  PRIMARY KEY (`T-id`),
  KEY `T-UserId` (`T-UserId`),
  CONSTRAINT `T-UserId` FOREIGN KEY (`T-UserId`) REFERENCES `user` (`U-id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of parttime
-- ----------------------------
INSERT INTO `parttime` VALUES ('1', '想做语文老师', '1', '2017-03-16 18:44:37');
INSERT INTO `parttime` VALUES ('2', '想做数学老师', '2', '2017-03-16 18:44:47');
INSERT INTO `parttime` VALUES ('3', '想做英语老师', '1', '2017-03-16 18:44:53');

-- ----------------------------
-- Table structure for province
-- ----------------------------
DROP TABLE IF EXISTS `province`;
CREATE TABLE `province` (
  `P-id` int(11) NOT NULL,
  `P-name` varchar(255) NOT NULL,
  PRIMARY KEY (`P-id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of province
-- ----------------------------
INSERT INTO `province` VALUES ('1', '河南');
INSERT INTO `province` VALUES ('2', '河北');

-- ----------------------------
-- Table structure for subject
-- ----------------------------
DROP TABLE IF EXISTS `subject`;
CREATE TABLE `subject` (
  `S-id` int(11) NOT NULL,
  `S-CourseId` int(11) NOT NULL,
  `S-UserId` int(11) DEFAULT NULL,
  PRIMARY KEY (`S-id`),
  KEY `S-CourseId` (`S-CourseId`),
  KEY `S-UserId` (`S-UserId`),
  CONSTRAINT `S-CourseId` FOREIGN KEY (`S-CourseId`) REFERENCES `course` (`Course-id`),
  CONSTRAINT `S-UserId` FOREIGN KEY (`S-UserId`) REFERENCES `user` (`U-id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of subject
-- ----------------------------
INSERT INTO `subject` VALUES ('1', '1', '1');
INSERT INTO `subject` VALUES ('3', '2', '2');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `U-id` int(11) NOT NULL,
  `U-photo` varchar(255) DEFAULT NULL,
  `U-sex` int(11) DEFAULT NULL,
  `U-NickName` varchar(255) DEFAULT NULL,
  `U-Password` varchar(255) DEFAULT NULL,
  `U-Email` varchar(255) DEFAULT NULL,
  `U-ProvinceId` int(11) NOT NULL,
  `U-CityId` int(11) NOT NULL,
  `U-TelePhone` varchar(255) NOT NULL,
  PRIMARY KEY (`U-id`),
  KEY `U-CityId` (`U-CityId`),
  KEY `C-ProvinceId` (`U-ProvinceId`),
  CONSTRAINT `C-ProvinceId` FOREIGN KEY (`U-ProvinceId`) REFERENCES `province` (`P-id`),
  CONSTRAINT `U-CityId` FOREIGN KEY (`U-CityId`) REFERENCES `city` (`C-id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', null, '1', '萍萍', '123456', '1234', '1', '1', '12454556');
INSERT INTO `user` VALUES ('2', null, '0', '小明', '12345', '1234', '2', '1', 'dddd');
INSERT INTO `user` VALUES ('3', '', '1', '小李', '23456', '45445', '1', '1', '2345566');
