package com.bmob.lostfound;

import com.bmob.lostfound.bean.User;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import cn.bmob.v3.Bmob;
import cn.bmob.v3.BmobUser;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.LogInListener;
import cn.bmob.v3.listener.RequestSMSCodeListener;



public class LoginOneKeyActivity extends BaseActivity implements OnClickListener{
	MyCountTimer timer;

	ImageView iv_left;

	TextView tv_title;
	
	EditText et_phone;

	EditText et_code;
	
	Button btn_send;
	
	Button btn_login;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_onekey);
        ButtonView();
        iv_left.setVisibility(View.VISIBLE);
        tv_title.setText("手机号一键登录");
	}
	private void ButtonView() {
	// TODO Auto-generated method stub
		iv_left=(ImageView)findViewById(R.id.iv_left);
		tv_title=(TextView)findViewById(R.id.tv_title);	
		et_phone=(EditText)findViewById(R.id.et_phone);	
		et_code=(EditText)findViewById(R.id.et_verify_code);	
		btn_send=(Button)findViewById(R.id.btn_send);	
		btn_login=(Button)findViewById(R.id.btn_login);	
		btn_login.setOnClickListener(this);
		btn_send.setOnClickListener(this);
		iv_left.setOnClickListener(this);
}
	/*
	@OnClick(R.id.iv_left)
	public void back(View view) {
		finish();
	}

	@OnClick(R.id.btn_login)
	public void login(View view) {
		oneKeyLogin();
	}*/

	private void oneKeyLogin() {
		// TODO Auto-generated method stub
		final String phone = et_phone.getText().toString();
		final String code = et_code.getText().toString();
		if (TextUtils.isEmpty(phone)) {
			ShowToast("手机号不能为空");
			//return;
		}

		if (TextUtils.isEmpty(code)) {
			ShowToast("验证码不能为空");
			//return;
		}
		//显示任务正在运行。。用这个
		final ProgressDialog progress = new ProgressDialog(LoginOneKeyActivity.this);
		progress.setMessage("正在登录");
		progress.setCanceledOnTouchOutside(false);//触摸到外面时就消失
		progress.show();
		Intent intent = new Intent(LoginOneKeyActivity.this,MainActivity.class);
		startActivity(intent);

	}
	private void requestSMSCode() {
		// TODO Auto-generated method stub
		String numberString = et_phone.getText().toString();
	
		if(!TextUtils.isEmpty(numberString)){
		
			timer =new MyCountTimer(60000, 1000);
			timer.start();
			
			Bmob.requestSMSCode(this, numberString,"Welcome", new RequestSMSCodeListener() {
				
				@Override
				public void done(Integer arg0, BmobException arg1) {
					// TODO Auto-generated method stub
					if(arg1==null){
						ShowToast("验证码发送成功");
						finish();
					}else {
						System.out.println(arg1);
						ShowToast("短信服务正在维护");
						timer.cancel();
					}
				}
			});
		}
		else{
			ShowToast("手机号不能为空adfasdfa"+numberString);
		}
	}

	class MyCountTimer extends CountDownTimer{

		public MyCountTimer(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onFinish() {
			// TODO Auto-generated method stub
			btn_send.setText("重新发送验证码");  
		}

		@Override
		public void onTick(long arg0) {
			// TODO Auto-generated method stub
			btn_send.setText((arg0 / 1000) +"后重发");  
		}
		
	}
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if(timer!=null){
			timer.cancel();
		}
	}
	@Override
	public void onClick(View arg0) {
		switch(arg0.getId()){
		case R.id.btn_login:
			ShowToast("点击btn_login");
			oneKeyLogin();
		break;
		case R.id.btn_send:
			ShowToast("点击btn_send");
			//跳转到注册界面
			requestSMSCode();
		break;
		case R.id.iv_left:
			ShowToast("点击btn_ivleft");
			finish();
			break;
		}
		
	}
	@Override
	public void setContentView() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void initViews() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void initListeners() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void initData() {
		// TODO Auto-generated method stub
		
	}
}
