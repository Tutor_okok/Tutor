package com.bmob.lostfound;




import com.bmob.lostfound.bean.User;


import cn.bmob.v3.listener.SaveListener;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class RegisterActivity extends BaseActivity implements android.view.View.OnClickListener {

	ImageView iv_left;
	
	TextView tv_title;
	
	EditText et_account;
	EditText et_password;
	
	EditText et_pwd_again;
	
	Button btn_register;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_register);
	    ButtonView();
	  
	    iv_left.setVisibility(View.VISIBLE);
	    tv_title.setText("RegisterActivity1");
	}
	private void ButtonView() {
	// TODO Auto-generated method stub
		et_password=(EditText)findViewById(R.id.et_password);
		iv_left=(ImageView)findViewById(R.id.iv_left);
		et_account=(EditText)findViewById(R.id.et_account);
		tv_title=(TextView)findViewById(R.id.tv_title);
		et_pwd_again=(EditText)findViewById(R.id.et_pwd_again);
		btn_register=(Button)findViewById(R.id.btn_register);
		
		iv_left.setOnClickListener(this);
		btn_register.setOnClickListener(this);
}
	
	public void back() {
		finish();
	}
	
	
	
	public void register() {
		registerUser();
	}
	private void registerUser() {
		// TODO Auto-generated method stub
		String account = et_account.getText().toString();
		String password = et_password.getText().toString();
		String pwd = et_pwd_again.getText().toString();
		if (TextUtils.isEmpty(account)) {
			ShowToast("用户名不能为空");
			return;
		}
		if (TextUtils.isEmpty(password)) {
			ShowToast("密码不能为空");
			return;
		}
		if (!password.equals(pwd)) {
			ShowToast("两次密码不一致");
			return;
		}
		final ProgressDialog progress = new ProgressDialog(RegisterActivity.this);
		progress.setMessage("请稍候。。。");
		progress.setCanceledOnTouchOutside(false);
		progress.show();
		final User user  = new User();
		
		user.setUsername(account);
		user.setPassword(password);
	
		user.signUp(this,new SaveListener() {
			
			@Override
			public void onSuccess() {
				// TODO Auto-generated method stub
				progress.dismiss();
				ShowToast("登陆成功"+user.getUsername());
				Intent intent = new Intent(RegisterActivity.this,MainActivity.class);
				intent.putExtra("from", "login");
				startActivity(intent);
				finish();
			}
			
			@Override
			public void onFailure(int arg0, String arg1) {
				// TODO Auto-generated method stub
				ShowToast("澶辫触:"+arg0+"澶辫触"+arg1);
				System.out.print(arg1);
			}
		});
};
		
		
	
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
		case R.id.iv_left:
			finish();
			break;
		case R.id.btn_register:
	        register();
			break;
	
		}
	}
	@Override
	public void setContentView() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void initViews() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void initListeners() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void initData() {
		// TODO Auto-generated method stub
		
	}


	
}
