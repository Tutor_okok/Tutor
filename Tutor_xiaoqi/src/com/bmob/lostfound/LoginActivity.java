package com.bmob.lostfound;



import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import cn.bmob.v3.Bmob;
import cn.bmob.v3.BmobUser;

import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.LogInListener;
import cn.bmob.v3.listener.SaveListener;


import com.bmob.lostfound.bean.User;

public class LoginActivity extends BaseActivity implements OnClickListener{
	public static String App_Id="9a2d82cb80d6fbcabbbbe8fe4099a7ab";
	private SharedPreferences spPreferences;
	private CheckBox is_savepswBox ;
    private CheckBox is_motologin ;
	
	Intent intent;
	ImageView iv_left;
	
	EditText et_account;
	
	EditText et_password;
	
	Button btn_login;
	
	Button btn_onekey;
	
	Button btn_register;
//	@InjectView(R.id.is_savepsw)
//	CheckBox is_savepswBox;
//	@InjectView(R.id.is_mutologin)
//	CheckBox is_motologin;
	
	@SuppressWarnings("deprecation")
	@Override
	protected  void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_login);
	    ButtonView();
	    Bmob.initialize(this,App_Id );
	    //Bmob基础类，包含初始化、获取服务器时间的方法,initialize是初始化服务器SDK
	    spPreferences = this.getSharedPreferences("userInfo", Context.MODE_WORLD_READABLE);
	    is_motologin = (CheckBox) findViewById(R.id.is_mutologin);
	    is_savepswBox = (CheckBox) findViewById(R.id.is_savepsw);
	    //这个东西做的是自动登录
	    is_motologin.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
				// TODO Auto-generated method stub
				if(is_motologin.isChecked()){
					spPreferences.edit().putBoolean("AUTO_ISCHECK", true).commit();
				}
				else{
					spPreferences.edit().putBoolean("AUTO_ISCHECK", false).commit();
				}
			}
		});
	    is_savepswBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
				// TODO Auto-generated method stub
				if(is_savepswBox.isChecked()){
					//这个做的是记住密码
					spPreferences.edit().putBoolean("ISCHECK", true).commit();
				}
				else{
					spPreferences.edit().putBoolean("ISCHECK", false).commit();
				}
			}
		});
	    
	    if(spPreferences.getBoolean("ISCHECK", false)){
	    	is_savepswBox.setChecked(true);
	    	et_account.setText(spPreferences.getString("USER_NAME", ""));
	    	et_password.setText(spPreferences.getString("PASSWORD", ""));
	    	if(spPreferences.getBoolean("AUTO_ISCHECK", false)){
	    		is_motologin.setChecked(true);
	    		Intent intent =  new Intent(LoginActivity.this,MainActivity.class);
	    		startActivity(intent);
	    	}
	    }
	   
	    
	}
	
	private void ButtonView() {
	// TODO Auto-generated method stub
		
		btn_login=(Button)findViewById(R.id.btn_login);
		btn_register=(Button)findViewById(R.id.btn_register);
		btn_onekey=(Button)findViewById(R.id.btn_onekey);
		et_account=(EditText)findViewById(R.id.et_account);
		et_password=(EditText)findViewById(R.id.et_password);
		iv_left=(ImageView)findViewById(R.id.iv_left);
		btn_login.setOnClickListener(this);
		btn_register.setOnClickListener(this);
		btn_onekey.setOnClickListener(this);
		iv_left.setOnClickListener(this);
		
}
	/*
	@OnClick(R.id.iv_left)
	public void back()
	{
		finish();
	}
	
	@OnClick(R.id.btn_login)
	public void login(View view) {
		login();
	}
	@OnClick(R.id.btn_register)
	public void register()
	{
		//跳转到注册界面
		Intent intent = new Intent(LoginActivity.this,RegisterActivity.class);
		startActivity(intent);
	}
	@OnClick(R.id.btn_onekey)
	public void onekey()
	{
		Intent intent = new Intent(LoginActivity.this,LoginOneKeyActivity.class);
		startActivity(intent);
	}
	*/
	private void login() {
		// TODO Auto-generated method stub
		final String account = et_account.getText().toString();
		final String password = et_password.getText().toString();
		if (TextUtils.isEmpty(account)) {
			ShowToast("账户不能为空");
			return;
		}
		if (TextUtils.isEmpty(password)) {
			ShowToast("密码不能为空");
			return;
		}
		final ProgressDialog progress = new ProgressDialog(LoginActivity.this);
		progress.setMessage("请稍候。。。。");
		progress.setCanceledOnTouchOutside(false);
		progress.show();
	    BmobUser.loginByAccount(this,account, password, new LogInListener<User>() {



			@Override
			public void done(User user, BmobException ex) {
				// TODO Auto-generated method stub
				// TODO Auto-generated method stub
				progress.dismiss();
				if(ex==null){
					ShowToast("登录成功"+user.getUsername());
					if(is_savepswBox.isChecked()){
						Editor editor = spPreferences.edit();
						editor.putString("USER_NAME", account);
						editor.putString("PASSWORD", password);
						editor.commit(); 
					}
					Intent intent = new Intent(LoginActivity.this,MainActivity.class);
					intent.putExtra("from", "login");
					startActivity(intent);		
					finish();
				}else{
					ShowToast("鐧婚檰澶辫触锛歝ode="+ex.getErrorCode()+"锛岄敊璇弿杩"+ex.getLocalizedMessage());
				}
				
			}
		});
	}

	@Override
	public void onClick(View arg0) {
		switch(arg0.getId()){
		case R.id.btn_login:
		login();
		break;
		case R.id.btn_register:
			//跳转到注册界面
		User user=new User();
			user.setUsername(et_account.getText().toString());
			user.setPassword(et_password.getText().toString());
			user.login(this, new SaveListener(){

				@Override
				public void onFailure(int arg0, String arg1) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void onSuccess() {
					// TODO Auto-generated method stub
					ShowToast("注册成功");
				}
				
				
			});
			intent = new Intent(LoginActivity.this,RegisterActivity.class);
			startActivity(intent);
		break;
		case R.id.btn_onekey:
			intent = new Intent(LoginActivity.this,LoginOneKeyActivity.class);
			startActivity(intent);
			break;
		case R.id.iv_left:
			finish();
			break;
		
		}
	}

	@Override
	public void setContentView() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void initViews() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void initListeners() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void initData() {
		// TODO Auto-generated method stub
		
	}

}
